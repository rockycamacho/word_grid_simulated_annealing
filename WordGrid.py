import random
import re
from pathlib import Path

from simanneal import Annealer


class Intersection:
    def __init__(self, x, y, line_a, line_b):
        self.x = x
        self.y = y
        self.line_a = line_a
        self.line_b = line_b

    def __str__(self):
        return '{0},{1} -> {2} | {3}'.format(self.x, self.y, self.line_a, self.line_b)

    def __repr__(self):
        return str(self)


class WordGrid:
    width = 0
    height = 0
    lines = []

    def __init__(self, width, height, lines):
        self.width = width
        self.height = height
        self.lines = lines
        self.intersections = []
        self.board = []
        for row in range(self.height):
            for col in range(self.width):
                self.board.append([' '] * self.width)
        for line in lines:
            if line.is_horizontal:
                for i in range(line.length):
                    self.board[line.y][line.x + i] = 'x'
            else:
                for i in range(line.length):
                    self.board[line.y + i][line.x] = 'x'
        for row in range(self.height):
            for col in range(self.width):
                if self.board[row][col] == 'x':
                    horizontal_line = None
                    vertical_line = None
                    for line in lines:
                        if line.is_horizontal and line.y == row and line.x <= col <= line.x + line.length:
                            horizontal_line = line
                        if not line.is_horizontal and line.x == col and line.y <= row <= line.y + line.length:
                            vertical_line = line
                    if horizontal_line is not None and vertical_line is not None:
                        self.intersections.append(Intersection(col, row, horizontal_line, vertical_line))

        print(self.intersections)

    def display(self):
        for row in range(self.height):
            print(' '.join(self.board[row]))
            # for col in range(self.width):

    def solve(self, words):
        solver = WordGridSolver(self, words, [i for i in range(len(words))])
        solver.set_schedule({
            'tmin': .25,
            'tmax': 2500000.0,
            'steps': 20000,
            'updates': 1000
        })
        final_state, score = solver.anneal()
        print(final_state)
        print(score)
        return score

    def apply(self, words):
        for line, word in zip(self.lines, words):
            if line.is_horizontal:
                for i in range(min(line.length, len(word))):
                    self.board[line.y][line.x + i] = word[i]
            else:
                for i in range(min(line.length, len(word))):
                    self.board[line.y + i][line.x] = word[i]

    def get_lines(self):
        return self.lines

    def get(self, x, y):
        return self.board[y][x]

    def get_intersections(self):
        return self.intersections


class Line:
    def __init__(self, x, y, length, is_horizontal):
        self.x = x
        self.y = y
        self.length = length
        self.is_horizontal = is_horizontal

    def __str__(self):
        orientation = 'DOWN'
        if self.is_horizontal:
            orientation = 'ACROSS'
        return '{0},{1} -> {2} {3}'.format(self.x, self.y, self.length, orientation)

    def __repr__(self):
        return str(self)


class GridParser:

    def parse(self, path):
        def by_length(elem):
            return elem.length

        text = Path(path).read_text()
        lines = []
        for space in text.split('\n'):
            values = space.split(' ')
            lines.append(Line(int(values[0]), int(values[1]), int(values[2]), values[3] == 'H'))
        lines.sort(key=by_length)
        return lines


class WordsParser:
    def parse(self, filepath):
        text = Path(filepath).read_text()
        words = [re.sub('[- \']', '', word) for word in text.split('\n')]
        words.sort(key=len)
        return words


class WordGridSolver(Annealer):
    def __init__(self, word_grid, word_set, initial_state):
        self.word_grid = word_grid
        self.word_set = word_set
        self.state = initial_state

    def move(self):
        line_lengths = set([line.length for line in self.word_grid.get_lines()])
        indexes = []
        while len(indexes) < 2:
            line_length = random.choice(list(line_lengths))
            indexes = [i for i in range(len(self.word_set)) if len(self.word_set[self.state[i]]) == line_length]
        a, b = random.sample(indexes, 2)
        self.state[a], self.state[b] = self.state[b], self.state[a]
        # print('swapped: {0} {1}'.format(self.word_set[self.state[a]], self.word_set[self.state[b]]))

    def energy(self):
        words = [self.word_set[self.state[i]] for i in range(len(self.state))]
        self.word_grid.apply(words)
        words_found = set()
        for line, word in zip(self.word_grid.get_lines(), words):
            if line.is_horizontal:
                inserted_word = ''.join(
                    [self.word_grid.get(line.x + i, line.y) for i in range(line.length)])
                if inserted_word in words:
                    words_found.add(inserted_word)
            else:
                inserted_word = ''.join(
                    [self.word_grid.get(line.x, line.y + i) for i in range(line.length)])
                if inserted_word in words:
                    words_found.add(inserted_word)
        # self.word_grid.display()
        # print()
        # print(words_found)
        fitness = len(self.word_set) - len(words_found)
        print('words_found: {0}'.format(str(words_found)))
        print('fitness: {0}'.format(fitness))
        if fitness == 0:
            self.user_exit = True
        return fitness

    def energy2(self):
        words = [self.word_set[self.state[i]] for i in range(len(self.state))]
        self.word_grid.apply(words)
        working_intersections = 0
        for intersection in self.word_grid.get_intersections():
            horizontal_word = ''.join([self.word_grid.get(intersection.line_a.x + i, intersection.line_a.y) for i in
                            range(intersection.line_a.length)])
            vertical_word = ''.join([self.word_grid.get(intersection.line_b.x, intersection.line_b.y + i) for i in
                                       range(intersection.line_b.length)])
            if horizontal_word in self.word_set:
                working_intersections += 1
            if vertical_word in self.word_set:
                working_intersections += 1

        fitness = len(self.word_grid.get_intersections()) * 2 - working_intersections
        print('fitness: {0}'.format(fitness))
        if fitness == 0:
            self.user_exit = True
        return fitness


