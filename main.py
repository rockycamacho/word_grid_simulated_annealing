import unittest

import WordGrid


class SolutionTest(unittest.TestCase):

    def solve_grid(self, grid_text_path, words_text_path):
        lines = WordGrid.GridParser().parse(grid_text_path)
        words = WordGrid.WordsParser().parse(words_text_path)
        print(words)
        word_grid = WordGrid.WordGrid(15, 15, lines)
        word_grid.display()
        score = word_grid.solve(words)
        word_grid.display()
        self.assertEqual(0, score)

    def test_display_word_grid(self):
        self.solve_grid(
            'input/word_grid1_grid.txt',
            'input/word_grid1_words.txt'
        )


    def test_display_word_grid2(self):
        self.solve_grid(
            'input/word_grid2_grid.txt',
            'input/word_grid2_words.txt'
        )


    def test_display_word_grid3(self):
        self.solve_grid(
            'input/word_grid3_grid.txt',
            'input/word_grid3_words.txt'
        )

    def test_display_word_grid4(self):
        self.solve_grid(
            'input/word_grid4_grid.txt',
            'input/word_grid4_words.txt'
        )

    def test_display_word_grid5(self):
        self.solve_grid(
            'input/word_grid5_grid.txt',
            'input/word_grid5_words.txt'
        )

    def test_display_word_grid6(self):
        self.solve_grid(
            'input/word_grid6_grid.txt',
            'input/word_grid6_words.txt'
        )